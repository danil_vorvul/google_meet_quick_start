# Google meet quick start new meeting

Endless quarantine meetings is suck, so I decide to automate starting new meeting  with pyautogui and open telegram. 
This is just for fun staff, but maybe someone will find it useful.
You can start meeting with google meet without changing this script (If your monitor is 1366x768 and Ubuntu 20.04 :D)

Also you can find here script, that will help you to set your own coordinates of applications
```
python mouse_cursor_position.py
```

## First step to run

You need to install dependencies:
```bash
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

And install needle staff for pyautogui
```
sudo apt-get install scrot
sudo apt-get install python3-tk
sudo apt-get install python3-dev
```

## Last step to run
If you have Ubuntu 20.04 and 1366x768 monitor,
you are logged in your google account so you are ready to run this :D
```
python start_meeting.py
```