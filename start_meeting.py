import pyautogui as pag


# You can stop execution if your mouse will jump to top side of monitor
pag.FAILSAFE = True

CHROME_IN_APP_PANEL = {"x": 27, "y": 743}
CHROME_SEARCH_BAR = {"x": 420, "y": 80}
GOOGLE_MEET_CREATE_MEETING_URL = "https://meet.google.com/new\n"

GOOGLE_MEET_DISABLE_CAMERA = {"x": 483, "y": 639}
GOOGLE_MEET_CONNECT = {"x": 1051, "y": 475}
GOOGLE_MEET_COPY_LINK = {"x": 698, "y": 415}

TELEGRAM_IN_APP_PANEL = {"x": 107, "y": 739}

pag.click(clicks=1, button="left", **CHROME_IN_APP_PANEL)

pag.moveTo(duration=0, **CHROME_SEARCH_BAR)
pag.click(clicks=1, button="left")

pag.typewrite(GOOGLE_MEET_CREATE_MEETING_URL, interval=0)

pag.moveTo(duration=7, **GOOGLE_MEET_DISABLE_CAMERA)
pag.click(clicks=1, button="left")

pag.moveTo(duration=1, **GOOGLE_MEET_CONNECT)
pag.click(clicks=1, button="left")

pag.moveTo(duration=3, **GOOGLE_MEET_COPY_LINK)
pag.click(clicks=1, button="left")

pag.moveTo(duration=0, **TELEGRAM_IN_APP_PANEL)
pag.click(clicks=1, button="left")
